advcharger hardware 

device for control MeanWell RST 4800 by CAN bus with power monitor LTC4151 and STM32G474CE mcu.

[schematic](doc/pcb/schematic.pdf)   
[gerber](doc/pcb/gerber)   
[hardware (Altium project)](hardware)  
[enclosure](encolsure)

![](doc/flowchart.png)

PCB view 

![](doc/photo/top.png)

With enclosure 

![](doc/photo/internal.png)


